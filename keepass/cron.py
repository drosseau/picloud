#!{bang_line}

# If the above line reads #!{bang_line} you need to manualy change it to be
# #!/path/to/your/python3/exe.

# This cronscript will update your local and remote keepass database files
# depending on which one is more recent and depending on your chosen
# ConflictStrategy. The main things you want to edit in here are:
#
#   - OWNCLOUD_SERVER
#   - OWNCLOUD_PASS
#   - OWNCLOUD_USER
#
# Read the documentation for ON_CONFLICT carefully if you change that value!
#
# The way this maintains the state is as follows:
# A file is saved in your picloud directory which contains SHA1 sums of both
# the local and remote database file. If a change is detected in the local
# file, but not the remote file, the local file will be uploaded to remote. If
# a change is detected in the remote file, but not the local file, the remote
# file will be downloaded and saved as your local file. If a change is detected
# IN BOTH action taken depends on the ON_CONFLICT variable.

import os
import sys
import ssl
import time
import json
import base64
import pathlib
import hashlib
import urllib.error
import urllib.request
import xml.etree.ElementTree as ET

from enum import Enum

# Owncloud info
OWNCLOUD_SERVER = "owncloud.local"
# BE SURE TO CHANGE TO YOUR USER
OWNCLOUD_USER = os.getenv("OWNCLOUD_USER", "")
# BE SURE TO CHANGE TO YOUR PASSWORD
OWNCLOUD_PASS = os.getenv("OWNCLOUD_PASS", "")

HOME = pathlib.Path.home()
# File name to put on the owncloud server and locally
KEEPASSFILE = ".keepassdb.kdbx"
# Set for the keepass db file
KEEPASSDB = os.path.join(HOME, KEEPASSFILE)


class ConflictStrategy(Enum):
    # Use the local version on conflict
    USE_LOCAL = 0
    # Use the local version on conflict, but backup the remote version locally
    USE_LOCAL_AND_BACKUP = 1
    # Use the remote version on conflict
    USE_REMOTE = 2
    # Use the remote version on conflict, but backup the local version locally
    USE_REMOTE_AND_BACKUP = 3
    # Fail on conflict
    FAIL = 4


# This is a strategy for dealing with conflicts. A conflict occurs when a
# change is detected in both the local version and the remote version.
# Conflicts _should_ be rare, but they may happen. The default is recommended.
ON_CONFLICT = ConflictStrategy.USE_LOCAL_AND_BACKUP

PICLOUD_DIR = os.path.join(HOME, ".local", "picloud")

# File to store shas
SHAFILE = os.path.join(PICLOUD_DIR, "shas.json")


# ==========================================
# = Typically don' need to edit below this =
# ==========================================

BACKUP_DIR = os.path.join(PICLOUD_DIR, "backups")

URLBASE = "https://{}".format(OWNCLOUD_SERVER)
WEBDAV = "{}/remote.php/webdav".format(URLBASE)
REMOTE_DB = "{}/{}".format(WEBDAV, KEEPASSFILE)
CA_FILE = os.path.join(PICLOUD_DIR, "cert.pem")

AUTH = base64.b64encode(
    "{}:{}".format(OWNCLOUD_USER, OWNCLOUD_PASS).encode()
).decode()


SSLCTX = ssl.create_default_context(cafile=CA_FILE)
SSLCTX.verify_mode = ssl.CERT_REQUIRED


def default_headers():
    return {
        "Authorization": "Basic {}".format(AUTH),
        "User-Agent": "picloud cron",
        "Accept": "*/*",
        "Host": OWNCLOUD_SERVER,
        "Connection": "keep-alive",
    }


def do_req(method, url, headers=None, data=None):
    h = default_headers()
    if headers is not None:
        h.update(headers)
    req = urllib.request.Request(url, headers=h, method=method, data=data)
    return urllib.request.urlopen(req, context=SSLCTX)


def get_last_updated_local():
    stat = os.stat(KEEPASSDB)
    return time.gmtime(stat.st_mtime)


def get_last_updated_remote():
    with do_req("PROPFIND", REMOTE_DB) as r:
        if r.code != 200 and r.code != 207:
            raise Exception("Bad propfind: {}".format(r.read().decode()))
        doc = ET.fromstring(r.read().decode())

    ns = {
        "d": "DAV:",
        "s": "http://sabredav.org/ns",
        "oc": "http://owncloud.org/ns",
    }
    mod_e = doc.find("./d:response/d:propstat/d:prop/d:getlastmodified", ns)
    if mod_e is None:
        return None
    # Thu, 12 Jul 2018 21:15:13 GMT
    mod_t = time.strptime(mod_e.text, "%a, %d %b %Y %H:%M:%S %Z")
    return mod_t


def get_local_sha():
    m = hashlib.sha1()
    with open(KEEPASSDB, "rb") as f:
        m.update(f.read())
    return m.hexdigest()


def get_remote_sha():
    try:
        with do_req("HEAD", REMOTE_DB) as r:
            if r.code != 200:
                raise Exception(
                    "unexpected response getting sha: {}".format(
                        r.read().decode()
                    )
                )
            chksums = r.getheader("OC-Checksum")
            if "," in chksums:
                chksum = chksums.split(",")[0]
            else:
                chksum = chksums
            return chksum.split(":")[1]
    except urllib.error.HTTPError as e:
        if e.code == 404:
            return None
        return ""


def upload_local():
    with open(KEEPASSDB, "rb") as f:
        body = f.read()
    headers = {
        "Content-Type": "application/octet-stream",
    }
    with do_req("PUT", REMOTE_DB, headers=headers, data=body) as r:
        if r.code != 204 and r.code != 201:
            body = r.read().decode()
            raise Exception("failed to upload {} {}".format(r.code, body))


def _get_remote():
    with do_req("GET", REMOTE_DB) as r:
        if r.code != 200:
            body = r.read().decode()
            raise Exception("failed to get remote {} {}".format(r.code, body))
        return r.read()


def download_remote():
    remote = _get_remote()
    with open(KEEPASSDB, "wb") as w:
        w.write(remote)


def backup_local():
    bkupfile = os.path.join(
        BACKUP_DIR,
        "local-{}.kdbx.backup".format(int(time.time()))
    )
    with open(KEEPASSDB, "rb") as f:
        with open(bkupfile, "wb") as w:
            w.write(f.read())


def backup_remote():
    remote = _get_remote()
    bkupfile = os.path.join(
        BACKUP_DIR,
        "remote-{}.kdbx.backup".format(int(time.time()))
    )
    with open(bkupfile, "wb") as f:
        f.write(remote)


def handle_conflict():
    if ON_CONFLICT == ConflictStrategy.FAIL:
        raise Exception("CONFLICT - FAIL")
    elif ON_CONFLICT == ConflictStrategy.USE_LOCAL:
        upload_local()
    elif ON_CONFLICT == ConflictStrategy.USE_LOCAL_AND_BACKUP:
        backup_remote()
        upload_local()
    elif ON_CONFLICT == ConflictStrategy.USE_REMOTE:
        download_remote()
    elif ON_CONFLICT == ConflictStrategy.USE_REMOTE_AND_BACKUP:
        backup_local()
        download_remote()
    else:
        raise Exception("CONFLICT - Unknown strategy")


class InfoFile:
    def __init__(self, d):
        try:
            local = d["local"]
            self.local_sha = local.get("sha", None)
        except KeyError:
            self.local_sha = None
        try:
            remote = d["remote"]
            self.remote_sha = remote.get("sha", None)
        except KeyError:
            self.remote_sha = None
        self.last_update = d.get("last_update", "N/A")

    def set_updated(self):
        self.last_update = time.strftime("%d/%m/%Y %H:%M:%S")

    def dump(self, fname=SHAFILE):
        with open(fname, "w") as f:
            json.dump({
                "local": {"sha": self.local_sha},
                "remote": {"sha": self.remote_sha},
                "last_update": self.last_update,
            }, f)


def _check_file(f):
    p = pathlib.Path(f)
    if not p.exists():
        raise Exception("{} doesn't exist".format(f))


_check_file(KEEPASSDB)


def _check_dir(d):
    p = pathlib.Path(d)
    if not p.exists():
        p.mkdir(parents=True)


_check_dir(BACKUP_DIR)


try:
    # Do the SHA calls first. This way we're just gonna get out of here if the
    # cert is wrong or something.
    remote_sha = get_remote_sha()
    local_sha = get_local_sha()

    # Get the info cache
    try:
        with open(SHAFILE, "r") as f:
            d = json.load(f)
    except FileNotFoundError as e:
        d = {}

    info = InfoFile(d)

    # There is no remote version, so we'll just update and get out
    if remote_sha is None:
        upload_local()
        info.local_sha = local_sha
        info.remote_sha = local_sha
        info.set_updated()
        info.dump()
    # Go through the whole process, if the shas are equal we can just get out.
    # If they aren't equal, we'll try to update.
    elif remote_sha != local_sha:
        local_changed = info.local_sha != local_sha
        remote_changed = info.remote_sha != remote_sha
        changed = local_changed or remote_changed
        if changed:
            if local_changed and remote_changed:
                handle_conflict()
                info.local_sha = local_sha
                info.remote_sha = remote_sha
            elif local_changed:
                upload_local()
                info.local_sha = local_sha
                info.remote_sha = local_sha
            elif remote_changed:
                download_remote()
                info.remote_sha = remote_sha
                info.local_sha = remote_sha
            info.set_updated()
            info.dump()
except Exception as e:
    print(e)
    sys.exit(1)
