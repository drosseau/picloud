#!/bin/bash

CRON_EXE="cron.py"
PICLOUD_DIR="$HOME/.local/picloud"
PYTHON3_EXEC=$(which python3)
if [ -z "$PYTHON3_EXEC" ]; then
    read -p "No executable found for python3. Install crontab entry anyway? [y/N] " yn
    case $yn in
        [Yy]*)
            "Installing crontab entry anway. Note that you will need to change the top of the file manually or the crontab entry invocation with \`crontab -e\`"
            ;;
        *)
            "Please install python3 and ensure it is somewhere in your PATH"
            exit 1
            ;;
    esac
fi

if [ ! -d "$PICLOUD_DIR" ] || [ ! -d "$PICLOUD_DIR/backups" ]; then
    mkdir -p "$PICLOUD_DIR/backups"
    chmod -R 700 "$PICLOUD_DIR"
fi

cp "$CRON_EXE" "$PICLOUD_DIR"

chmod 700 "$CRON_EXE"

# Make the python path play nicely with sed
PYTHON3_EXEC=${PYTHON3_EXEC//\//\\/}

sed -i.bak -e "s/^#!{bang_line}\$/#!$PYTHON3_EXEC/g" "$PICLOUD_DIR/$CRON_EXE"

echo "$CRON_EXE script moved to $PICLOUD_DIR, be sure to set the proper variables in that script or else it won't work"

cron_entry="*/15 * * * * $PICLOUD_DIR/$CRON_EXE"

(crontab -l 2>/dev/null; echo "$cron_entry") | crontab
