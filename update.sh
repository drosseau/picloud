#!/bin/bash

# Updates your owncloud version, uses the OWNCLOUD_VERSION config variable
# to get the new version

source helpers.sh

backup_dir=/opt/backup
owncloud_dir="$OWNCLOUD_ROOT/owncloud"
owncloud_backup="$backup_dir/owncloud.bak"
mysql_backup="$backup_dir/owncloud_sql.bak"
config_backup="$owncloud_backup/config/config.php"
data_backup="$owncloud_backup/data"
apps_backup="$owncloud_backup/apps"

if [ ! -d "$backup_dir" ]; then
    sudo mkdir -p "$backup_dir"
    sudo chmod -R 770 "$backup_dir"
fi

info "Entering maintenance mode and restarting nginx"
# Stop nginx
sshi "sudo systemctl stop nginx.service"
# Enter maintenance mode
sshi "sudo -u www-data php $owncloud_dir/occ maintenance:mode --on"
exit_error "$?" "failed to enter maintenance mode"

# Backup everything
sshi "sudo mv '$owncloud_dir' '$owncloud_backup'"
exit_error "$?" "failed to backup $owncloud_dir"
sshi "sudo mysqldump -sfu root owncloud > $mysql_backup"

install_owncloud

# Move config
sshi "sudo cp '$config_backup' '$owncloud_dir/config/config.php' && sudo chown www-data:www-data '$owncloud_dir/config/config.php"
# move $owncloud_dir/data, but it might not have been used anyway
sshi "sudo cp -r '$data_backup' '$owncloud_dir/data' && sudo chown -R www-data:www-data '$owncloud_dir/data'"
# apps
sshi "sudo cp -r '$apps_backup' '$owncloud_dir/apps' && sudo chown -R www-data:www-data '$owncloud_dir/apps'"

if [ -n "$DELETE_OWNCLOUD_UPDATE_BACKUP" ] && [ "$DELETE_OWNCLOUD_UPDATE_BACKUP" -eq "1" ]; then
    sshi "sudo rm -r '$owncloud_backup'"
    sshi "sudo rm '$mysql_backup'"
else
    info "Didn't delete owncloud backups at $backup_dir"
fi

info "Performing post install upgrade..."
sshi "sudo -u www-data php $owncloud_dir/occ upgrade"

sshi "sudo -u www-data php $owncloud_dir/occ maintenance:mode --off"
exit_error "$?" "failed to leave maintenance mode"

info "Update successful, exiting maintenance mode and restarting nginx"
# Start nginx
sshi "sudo systemctl start nginx.service"
