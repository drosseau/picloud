# PiCloud

PiCloud is a collection of portable shell scripts to make your own OwnCloud server very easily on a Raspberry Pi. Everything runs over SSH. These scripts should run fine on anything Linux with bash or macOS.

# Setup SSH

The quickest way to enable SSH to add a file named `ssh` to your boot partition of the sd card as docmented [here](https://www.raspberrypi.org/documentation/remote-access/ssh/). Otherwise you can login and do it with `raspi-config`. This gives an insecure ssh though (the default user/pw are well known to be pi/raspberry). The next step will be to run the `ssh.sh` script to generate a key and store it so you can login easily without a password. Note that without ssh setup you cannot run the `setup.sh` script.


# OwnCloud Setup

After running the `ssh.sh` script (or manually setting up ssh with keys) go over to the `setup_config.sh` script and check out the rest of the config options. The script will do almost everything necessary and it will do it all over ssh.

After running the script you will have an OwnCloud instance running on your Pi behind an nginx proxy. By default, self signed certificates are created with OpenSSL, so your browser will most likely give you a security warning. The server's cert is copied locally to the `PICLOUD_DIR` directory as `cert.pem` for convenience.

# Keepass Setup

You can use the scripts included in the `keepass` directory to set up a cron job that will upload your database when it detects a change (by default it runs every 15 minutes). Some variables need to be set in the `cron.sh` script.

# TODO

 - Add external harddrive setup
