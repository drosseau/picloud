#!/bin/bash

source helpers.sh

if [ -z "$PI_IP" ]; then
	exit_error "1" "need to set the PI_IP variable in setup_config.sh"
fi

dir=$(dirname "$SSH_KEY_FILE")

if [ ! -d "$dir" ]; then
	mkdir -p "$dir"
fi

info "Generating an ssh key"
# Set up your Raspberry Pi to run an ssh server
debug "ssh-keygen -f \"$SSH_KEY_FILE\" -q -N '' -t \"$SSH_KEY_TYPE\""

ssh-keygen -f "$SSH_KEY_FILE" -q -N '' -t "$SSH_KEY_TYPE"

info "Copying key to Pi.. If this is your first time using ssh on your Pi it will ask you to verifiy the host. In most cases you can just say 'yes', but if you want to verify it yourself do so now and then restart the script. You will be asked for a password, it defaults to 'raspberry'"
debug "scp \"$SSH_KEY_FILE.pub\" ${SSH_USER}@${PI_IP}:/tmp/pub_key"

# Add public key to pi
scp "$SSH_KEY_FILE.pub" ${SSH_USER}@${PI_IP}:/tmp/pub_key

exit_error $? "failed to copy key to pi"

info "Copying key to the Pi's authorized_keys file. You will be asked for a password again"
cmd=$(cat <<EOF
if [ ! -d "~/.ssh" ]; then
	mkdir -p ~/.ssh && chmod 0700 ~/.ssh;
fi && if [ ! -f "~/.ssh/authorized_keys" ]; then
	touch ~/.ssh/authorized_keys && chmod 0600 ~/.ssh/authorized_keys;
fi && cat /tmp/pub_key >> ~/.ssh/authorized_keys && rm /tmp/pub_key
EOF
)
debug "ssh ${SSH_USER}@${PI_IP} $cmd"

# Append it to authorized_keys and then delete the /tmp one
ssh ${SSH_USER}@${PI_IP} "$cmd"

exit_error $? "failed to copy key on pi to authorized_keys file"

if [ -n "$SSH_ADD_TO_CONFIG" ] && [ "$SSH_ADD_TO_CONFIG" -gt "0" ]; then
	info "Adding options to $HOME/.ssh/config"
	if [ ! -d "$HOME/.ssh" ]; then
		mkdir "$HOME/.ssh"
	fi
	debug "writing to ~/.ssh/config"
	cat <<EOF >> ~/.ssh/config

# Auto generated Raspberry Pi setup
Host $PI_IP
User pi
IdentityFile $SSH_KEY_FILE
EOF

	if [ -n "$SERVER_NAME" ]; then
		if [ "$SERVER_NAME" != "$PI_IP" ]; then
			cat <<EOF >> ~/.ssh/config

# Auto generated Raspberry Pi setup
Host $SERVER_NAME
User pi
IdentityFile $SSH_KEY_FILE
EOF
		fi
	fi
fi

if [ -n "$SSH_NO_PASSWORDS" ] && [ "$SSH_NO_PASSWORDS" -gt "0" ]; then
	# Disable password authentication on Pi
	info "Turning off password based authentication and restarting the ssh server"
	cmd="sudo sed -i 's/^#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config"
	ssh ${SSH_USER}@${PI_IP} "$cmd"
	ssh ${SSH_USER}@${PI_IP} "sudo systemctl restart ssh.service"
fi
info "Done!"
