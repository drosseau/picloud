#!/bin/bash

# All used variables will be here
source helpers.sh

if [ -z "$DB_PASSWORD" ]; then
	error "set DB_PASSWORD password"
	exit 1
fi

if [ -z "$ADMIN_PASSWORD" ]; then
	error "set ADMIN_PASSWORD password"
	exit 1
fi

info "All commands run via \`ssh -i \"$SSH_KEY_FILE\" ${SSH_USER}@${PI_IP} \"\$cmd\"\`"

info "Updating apt cache"
sshi 'sudo DEBIAN_FRONTEND=noninteractive apt-get -q -y update'
exit_error $? "failed apt-get update"

info "Getting required packages"
cmd=$(cat <<EOF
sudo DEBIAN_FRONTEND=noninteractive apt-get -q -y install \
	 nginx openssl mariadb-server \
    php7.0-gd php7.0-json php7.0-mysql php7.0-curl \
    php7.0-intl php7.0-mcrypt php-imagick php7.0-fpm \
    php7.0-zip php7.0-xml php7.0-mbstring wget gpg
EOF
)
sshi "$cmd"

exit_error $? "failed to install required packages"

# Setup a static ip
if [ -n "$STATIC_IP" ]; then
	info "Setting up static ip: $STATIC_IP"

	cmd=$(cat <<REALEOF
cat <<EOF | sudo tee /etc/network/interfaces >/dev/null
source /etc/network/interfaces.d/*

auto lo
iface lo inet loopback

auto $INTERFACE
iface $INTERFACE inet static
	address $STATIC_IP
	netmask $NETMASK
	gateway $GATEWAY
EOF
REALEOF
)
	sshi "$cmd"
	exit_error $? "failed to write new /etc/network/interfaces file"
fi

if [ ! -d "$PICLOUD_DIR" ]; then
	mkdir -p "$PICLOUD_DIR"
	exit_error $? "failed to create dir: $PICLOUD_DIR"
fi

setup_cert

info "Writing conf file"
server_config_file=/etc/nginx/sites-available/owncloud-nginx
sshi "if [ ! -f \"$server_config_file\" ]; then sudo touch \"$server_config_file\"; fi"
exit_error $? "failed to create $server_config_file"
sshi "sudo rm /etc/nginx/sites-enabled/default 2>/dev/null"
sites_enabled_link=/etc/nginx/sites-enabled/owncloud-nginx
sshi "sudo ln -sf $server_config_file $sites_enabled_link"
exit_error $? "failed to link $server_config_file into /etc/nginx/sites-enabled"

server_name_entries=
if [ -n "$STATIC_IP" ]; then
	server_name_entries="\tserver_name $STATIC_IP;\n"
fi
if [ -n "$SERVER_NAME" ]; then
	if [ "$SERVER_NAME" != "$STATIC_IP" ]; then
		server_name_entries="$server_name_entries\tserver_name $SERVER_NAME;\n"
	fi
fi
server_name_entries=$(echo -e $server_name_entries)
cmd=$(cat <<REALEOF
cat <<'EOF' | sudo tee $server_config_file > /dev/null
upstream php-handler {
	server unix:/var/run/php/php7.0-fpm.sock;
}

server {
	listen 80;
$server_name_entries
	return 301 https://\$host\$request_uri;
}

server {
	listen 443 ssl;
$server_name_entries

	ssl_certificate /etc/nginx/cert.pem;
	ssl_certificate_key /etc/nginx/key.pem;

	root $OWNCLOUD_ROOT/owncloud;

	client_max_body_size $MAX_UPLOAD_SIZE;
	fastcgi_buffers 64 4K;

	gzip $GZIP;

	rewrite ^/caldav(.*)$ /remote.php/caldav\$1 redirect;
	rewrite ^/carddav(.*)$ /remote.php/carddav\$1 redirect;
	rewrite ^/webdav(.*)$ /remote.php/webdav\$1 redirect;

	index index.php;
	error_page 403 /core/templates/403.php;
	error_page 404 /core/templates/404.php;

	location ~ ^/(?:\.htaccess|data|config|db_structure\.xml|README){
		deny all;
	}

	location / {
		# The following 2 rules are only needed with webfinger
		rewrite ^/.well-known/host-meta /public.php?service=host-meta last;
		rewrite ^/.well-known/host-meta.json /public.php?service=host-meta-json last;

		rewrite ^/.well-known/carddav /remote.php/carddav/ redirect;
		rewrite ^/.well-known/caldav /remote.php/caldav/ redirect;

		rewrite ^(/core/doc/[^\/]+/)$ \$1/index.html;

		try_files \$uri \$uri/ =404;
}

	location ~ \.php(?:$|/) {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		include fastcgi_params;
		fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
		fastcgi_param PATH_INFO \$fastcgi_path_info;
		fastcgi_param HTTPS on;
		fastcgi_pass php-handler;
		fastcgi_intercept_errors on;
	}

	# Adding the cache control header for js and css files
	# Make sure it is BELOW the location ~ \.php(?:$|/) { block
	location ~* \.(?:css|js)$ {
		add_header Cache-Control "public, max-age=7200";
		# Add headers to serve security related headers
		add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload;";
		add_header X-Content-Type-Options nosniff;
		add_header X-Frame-Options "SAMEORIGIN";
		add_header X-XSS-Protection "1; mode=block";
		add_header X-Robots-Tag none;
		# Optional: Don't log access to assets
		access_log off;
	}

	# Optional: Don't log access to other assets
	location ~* \.(?:jpg|jpeg|gif|bmp|ico|png|swf)$ {
		access_log off;
	}
}
EOF
REALEOF
)

sshi "$cmd"
exit_error $? "failed to write nginx config"

install_owncloud

info "Setting up MariaDB"
# Setup MariaDB
cmd=$(cat <<REALEOF
sudo mysql -sfu root <<EOF
-- Drop anonymous users
DROP USER ''@'localhost';
-- Drop anonymous users for hostname too
DROP USER ''@'\$(hostname)';
-- Drop the test database
DROP DATABASE IF EXISTS test;
-- Don't allow logins that aren't from localhost for root
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
-- Delete the $DB_DATABASE database if it is already there
DROP DATABASE IF EXISTS $DB_DATABASE;
-- Create our owncloud database
CREATE DATABASE $DB_DATABASE;
CREATE USER $DB_USER@localhost IDENTIFIED BY '$DB_PASSWORD';
GRANT ALL PRIVILEGES ON $DB_DATABASE.* TO $DB_USER@localhost IDENTIFIED BY '$DB_PASSWORD';
FLUSH PRIVILEGES;
EOF
REALEOF
)

sshi "$cmd"
exit_error $? "failed to setup MariaDB"

occ_file="$OWNCLOUD_ROOT/owncloud/occ"

info "Finishing OwnCloud installation (this may take a while)"

cmd=$(cat <<EOF
sudo -u www-data php $occ_file maintenance:install \
-vv \
--database mysql \
--database-name '$DB_DATABASE' \
--database-user '$DB_USER' \
--database-pass '$DB_PASSWORD' \
--admin-user '$ADMIN_USER' \
--admin-pass '$ADMIN_PASSWORD' \
--data-dir '$OWNCLOUD_DATA_DIR'
EOF
)

sshi "$cmd"

info "Updating owncloud config file"

trusted_domains_push=
if [ -n "$STATIC_IP" ]; then
	trusted_domains_push="array_push(\$arr, '$STATIC_IP');\n"
fi
if [ -n "$SERVER_NAME" ]; then
	if [ "$SERVER_NAME" != "$STATIC_IP" ]; then
		trusted_domains_push="$trusted_domains_push\tarray_push(\$arr, '$SERVER_NAME');\n"
	fi
fi

sshi "if [ ! -d '$OWNCLOUD_DATA_DIR' ]; then sudo mkdir -p '$OWNCLOUD_DATA_DIR' && sudo chown www-data:www-data '$OWNCLOUD_DATA_DIR' && sudo chmod 770 '$OWNCLOUD_DATA_DIR'; fi"

trusted_domains_push=$(echo -e $trusted_domains_push)
config_file="$OWNCLOUD_ROOT/owncloud/config/config.php"
new_config_file="$OWNCLOUD_ROOT/owncloud/config/config.new.php"
cmd=$(cat <<OUTEREOF
sudo php << 'EOF' && sudo mv "$config_file" "$config_file.bak" && sudo mv "$new_config_file" "$config_file" && sudo chown www-data "$config_file" && sudo chmod 660 "$config_file"
<?php
	require '$config_file';
	\$arr = array();
	$trusted_domains_push
	\$CONFIG["trusted_domains"] = \$arr;
	\$newConfig = var_export(\$CONFIG, true);
	\$f = fopen('$new_config_file', 'w');
	fwrite(\$f, "<?php\\n\\\$CONFIG = ".\$newConfig.";\\n");
	fclose(\$f);
?>
EOF
OUTEREOF
)
sshi "$cmd"
warn_if_failed $? "failed to update owncloud config (must be done manually!)"

info "Adding user: $MY_USER"
cmd="export OC_PASS='$MY_USER_PASSWORD'; sudo -E -u www-data php $occ_file user:add --password-from-env '$MY_USER'"
sshi "$cmd"

if [ "$OWNCLOUD_ENCRYPT" -eq "1" ]; then
    sshi "sudo -u www-data php $occ_file app:enable encryption"
    sshi "sudo -u www-data php $occ_file encryption:enable"
    sshi "sudo -u www-data php $occ_file encryption:select-encryption-type $OWNCLOUD_ENCRYPTION_TYPE"
else
    warn "encryption is not enabled"
fi

if [ -z "$SERVER_NAME" ]; then
	SERVER_NAME=$STATIC_IP
fi

info "restarting networking to use static ip: $STATIC_IP"
sshi "sudo systemctl restart networking.service"
exit_error $? "failed to restart networking"

info "rebooting"
sshi "sudo reboot"

if [ -n "$MODIFY_HOSTS_FILE" ] && [ "$MODIFY_HOSTS_FILE" -gt "0" ]; then
    if [ "$STATIC_IP" != "$SERVER_NAME" ]; then
        hosts_line="$STATIC_IP $SERVER_NAME"
        found=
        # Check to see if we already have the line
        while read -r line; do
            if [ "$line" == "$hosts_line" ]; then
                found=1
                break
            fi
        done < "$HOSTS_FILE"
        if [ -z "$found" ]; then
            if [ "$EUID" -ne "0" ]; then
                echo "Asking to modify hosts file requires root access. Running command with sudo"
                echo "$hosts_line" | sudo tee -a "$HOSTS_FILE" > /dev/null
            else
                echo "$hosts_line" >> "$HOSTS_FILE"
            fi
        fi
    else
        warn "Asked to modify the hosts file, but STATIC_IP and SERVER_NAME are the same ($STATIC_IP == $SERVER_NAME)!"
    fi
fi

info "OwnCloud setup complete!"

cat <<EOF
You can view it in your browser at https://$SERVER_NAME

== Database ==
db: $DB_DATABASE
user: $DB_USER
pw: $DB_PASSWORD
host: localhost:3306

== Admin User ==
name: $ADMIN_USER
pw: $ADMIN_PASSWORD

== General User ==
name: $MY_USER
pw: $MY_USER_PASSWORD
EOF
