# Many of the defaults in here are sane and don't need to be changed

if [ -z "$_PICLOUD_SETUP_VARS" ]; then

# MUST BE SET
export MY_USER=
export MY_USER_PASSWORD=
export DB_USER=owncloud
export DB_PASSWORD=
export ADMIN_USER=admin
export ADMIN_PASSWORD=

# Database setup
export DB_DATABASE=owncloud

# SSH setup
# Most of these are just fine as the default
export SSH_KEY_FILE="$HOME/.ssh/id_rsa_raspberrypi"
export SSH_KEY_TYPE=rsa
export SSH_USER=pi
# Add some options to your ~/.ssh/config file to make things easier
export SSH_ADD_TO_CONFIG=1

# Don't allow password login at all
export SSH_NO_PASSWORDS=1

# This IP needs to be set manually. It is the IP given to your Pi dynamically
# by your router when it first starts up. Eventually we will set the IP to a
# static one defined by STATIC_IP, but this needs to be set first
#PI_IP=192.168.1.47 # Example
export PI_IP=192.168.1.233

# The Pi needs a static IP, make sure it is one that won't be given out to
# to other computers on your network. When in doubt just make sure that last
# number is big (but < 255 of course)
export STATIC_IP=${PI_IP}
# If the static IP is set these MUST be set
# If you don't know how to set a netmask just think of the numbers that are
# allowed to change for your IPs, for instance if you have 192.168.1.0-255
# then the netmask should be 255.255.255.0
export NETMASK=255.255.255.0
# This is the IP of your router. It will typically be in the lower end of your
# available IPs
export GATEWAY=192.168.1.2
# This probably does not need to be changed as it is the default for Raspbian
export INTERFACE=eth0

# This can be either a static IP or a hostname if you want to be clever
# (it is used in nginx's `server_name` field)
#
# To use this you'll need to modify your /etc/hosts file with something like:
#
# `echo "$STATIC_IP" "$SERVER_NAME" >> /etc/hosts`
export SERVER_NAME=owncloud.local

# Modify your hosts file automatically as shown above. Set this to empty if you
# don't want to do that. Note that to modify the hosts file this script needs
# to be run as root.
export MODIFY_HOSTS_FILE=1

# Hosts file to modify if MODIFY_HOSTS_FILE is set
export HOSTS_FILE=/etc/hosts

# Note that for /var/www/owncloud this will just be /var/www
export OWNCLOUD_ROOT=/var/www
# Set your version, this is the most recent version at the time this script
# was last updated
export OWNCLOUD_VERSION=10.2.0

# When updating, delete the backup on successful update
# To enable, set this to 1. By default, this is disabled since the update
# process may have issues.
export DELETE_OWNCLOUD_UPDATE_BACKUP=

# Where files are actually stored
export OWNCLOUD_DATA_DIR="/var/owncloud/data"

export OWNCLOUD_ENCRYPT=1
export OWNCLOUD_ENCRYPTION_TYPE="user-keys"
#export OWNCLOUD_ENCRYPTION_TYPE="masterkey"

export MAX_UPLOAD_SIZE=10G

# Use GZIP (setting to off will avoid removing ETag headers)
export GZIP=on

# SSL Setup for a self signed certificate. None of this matters much except
# for aesthetic reasons beyond the lifetime and common name
export SSL_CERT_LIFETIME=365
export SSL_COMMON_NAME=owncloud.local
export SSL_COUNTRY=US
export SSL_STATE=State
export SSL_LOCALITY=City
export SSL_ORG="picloud"
# Directory for the SSL cert. Default is probably fine.
export SSL_CERT_DIR=/etc/nginx

# Set to anything other than empty to get a ton of debug output
export DEBUG=1
#export DEBUG=1

# Directory to store picloud related files
export PICLOUD_DIR="$HOME/.local/picloud"

export _PICLOUD_SETUP_VARS=sourced
fi
