if [ -z "$_PICLOUD_HELPERS" ]; then

    source setup_config.sh

exit_error() {
	if [ "$1" -ne "0" ]; then
		error "$2"
		exit 1
	fi
}

warn_if_failed() {
	if [ "$1" -ne "0" ]; then
		warn "$2"
	fi
}

warn() {
	echo -e "\033[1;33m[WARN]: ${1}\033[00m"
}

error() {
	echo -e "\033[0;31m[ERR]: ${1}\033[0m"
}

info() {
	echo -e "\033[0;36m[INFO]: ${1}\033[0m"
}

debug() {
	if [ -n "$DEBUG" ]; then
		echo -e "\033[1;35m[DEBUG]: ${1}\033[00m"
	fi
}

sshi() {
	debug "ssh -i "'"'"$SSH_KEY_FILE"'"'" ${SSH_USER}@${PI_IP} "'"'"$1"'"'""
	ssh -i "$SSH_KEY_FILE" ${SSH_USER}@${PI_IP} "$1"
}

scpi_get() {
	scp -i "$SSH_KEY_FILE" "${SSH_USER}@${PI_IP}:$1" "$2"
}

setup_cert() {
    info "Using SSH to create a certificate on the Pi"
    
    sshi "if [ ! -d \"$SSL_CERT_DIR\" ]; then sudo mkdir -p \"$SSL_CERT_DIR\"; fi"
    exit_error $? "failed to make $SSL_CERT_DIR"
    ssl_cert="$SSL_CERT_DIR/cert.pem"
    ssl_key="$SSL_CERT_DIR/key.pem"
    cmd=$(cat <<EOF
sudo openssl req -new -newkey rsa -days $SSL_CERT_LIFETIME -nodes -x509 \
	-out $ssl_cert -keyout $ssl_key \
    -subj '/C=$SSL_COUNTRY/ST=$SSL_STATE/L=$SSL_LOCALITY/O=$SSL_ORG/CN=$SSL_COMMON_NAME'
EOF
)
    sshi "$cmd"
    exit_error $? "failed to generate a certificate"
    
    sshi "sudo cp $ssl_cert /tmp/cert.pem && sudo chmod 644 /tmp/cert.pem"
    
    info "Copying cert to $PICLOUD_DIR/cert.pem"
    scpi_get "/tmp/cert.pem" "$PICLOUD_DIR/cert.pem"
    exit_error $? "failed to get cert"
    
    sshi "sudo rm /tmp/cert.pem"
    
    sshi "sudo chmod 0600 $ssl_cert $ssl_key"
    exit_error $? "failed to change permissions on cert and key"
    
    if [ -n "$1" ] && [ "$1" -eq "1" ]; then
        restart_nginx
    fi
}

restart_nginx() {
    info "Restarting nginx"
    sshi "sudo systemctl restart nginx.service"
    exit_error $? "failed to restart nginx"
}

install_owncloud() {
    download_url=https://download.owncloud.org/community/owncloud-${OWNCLOUD_VERSION}.tar.bz2
    owncloud_tar=/tmp/owncloud.tar.bz2
    
    info "Getting $download_url"
    sshi "wget --quiet -O $owncloud_tar $download_url > /dev/null"
    exit_error $? "failed to download owncloud"

    sshi "if [ ! -d \"$OWNCLOUD_ROOT\" ]; then sudo mkdir -p \"$OWNCLOUD_ROOT\"; fi"
    exit_error $? "failed to create $OWNCLOUD_ROOT"
    
    info "Unpacking OwnCloud (this could take a while)"
    sshi "sudo tar xjf $owncloud_tar -C '$OWNCLOUD_ROOT' > /dev/null"
    exit_error $? "failed to unpack owncloud tar"
    sshi "sudo chown -R www-data:www-data $OWNCLOUD_ROOT/owncloud"
    
    if [ -n "$1" ] && [ "$1" -eq "1" ]; then
        restart_nginx
    fi
    sshi "rm \"$owncloud_tar\""
}

export _PICLOUD_HELPERS=sourced
fi
